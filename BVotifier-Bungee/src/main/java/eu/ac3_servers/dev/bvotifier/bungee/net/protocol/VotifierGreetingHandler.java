package eu.ac3_servers.dev.bvotifier.bungee.net.protocol;

import eu.ac3_servers.dev.bvotifier.bungee.BVotifier;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.StandardCharsets;

/**
 * Handles the Votifier greeting.
 */
public class VotifierGreetingHandler extends ChannelInboundHandlerAdapter
{

	@Override
	public void channelActive( ChannelHandlerContext ctx ) throws Exception
	{

		String version = "VOTIFIER " + BVotifier.getInstance().getVersion() + "\n";
		ctx.writeAndFlush( Unpooled.copiedBuffer( version, StandardCharsets.UTF_8 ) );

	}

}
