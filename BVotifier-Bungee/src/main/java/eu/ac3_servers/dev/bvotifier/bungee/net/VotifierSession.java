package eu.ac3_servers.dev.bvotifier.bungee.net;

import io.netty.util.AttributeKey;

public class VotifierSession
{
	public static final AttributeKey<VotifierSession> KEY = AttributeKey.valueOf( "votifier_session" );
	private ProtocolVersion version = ProtocolVersion.UNKNOWN;

	public ProtocolVersion getVersion()
	{
		return version;
	}

	public void setVersion( ProtocolVersion version )
	{
		if ( this.version != ProtocolVersion.UNKNOWN ) {
			throw new IllegalStateException( "Protocol version already switched" );
		}

		this.version = version;
	}

	public enum ProtocolVersion
	{
		UNKNOWN,
		ONE
	}
}
