package eu.ac3_servers.dev.bvotifier.bungee.net.protocol;

import com.vexsoftware.votifier.crypto.RSA;
import eu.ac3_servers.dev.bvotifier.bungee.BVotifier;
import eu.ac3_servers.dev.bvotifier.bungee.model.Vote;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.CorruptedFrameException;

import java.util.List;

/**
 * Decodes original protocol votes.
 */
public class VotifierProtocol1Decoder extends ByteToMessageDecoder
{
	private static final boolean WARNING = false;

	/**
	 * Reads a string from a block of data.
	 *
	 * @param data The data to read from
	 * @return The string
	 */
	private static String readString( byte[] data, int offset )
	{
		StringBuilder builder = new StringBuilder();
		for ( int i = offset; i < data.length; i++ ) {
			if ( data[ i ] == '\n' ) {
				break; // Delimiter reached.
			}
			builder.append( (char) data[ i ] );
		}
		return builder.toString();
	}

	@Override
	protected void decode( ChannelHandlerContext ctx, ByteBuf buf, List<Object> list ) throws Exception
	{
		int readable = buf.readableBytes();

		if ( readable < 256 ) {
			return;
		}

		byte[] block = new byte[ 256 ];
		buf.getBytes( 0, block );

		try {
			block = RSA.decrypt( block, BVotifier.getInstance().getKeyPair().getPrivate() );
		} catch ( Exception e ) {
			throw new CorruptedFrameException( "Could not decrypt data", e );
		}
		int position = 0;

		String opcode = readString( block, position );
		position += opcode.length() + 1;
		if ( !opcode.equals( "VOTE" ) ) {
			throw new CorruptedFrameException( "VOTE opcode not found" );
		}

		// Parse the block.
		String serviceName = readString( block, position );
		position += serviceName.length() + 1;
		String username = readString( block, position );
		position += username.length() + 1;
		String address = readString( block, position );
		position += address.length() + 1;
		String timeStamp = readString( block, position );
		position += timeStamp.length() + 1;

		final Vote vote = new Vote( serviceName, username, address, timeStamp );

		if ( BVotifier.getInstance().isDebug() ) {
			BVotifier.getInstance().getLogger().info( "Received protocol v1 vote record -> " + vote );
		}

		list.add( vote );

		ctx.pipeline().remove( this );

	}
}
