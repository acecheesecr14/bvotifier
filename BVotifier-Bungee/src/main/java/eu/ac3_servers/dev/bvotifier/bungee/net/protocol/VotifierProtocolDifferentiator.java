package eu.ac3_servers.dev.bvotifier.bungee.net.protocol;

import eu.ac3_servers.dev.bvotifier.bungee.net.VotifierSession;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.CorruptedFrameException;

import java.util.List;

/**
 * Attempts to determine if original protocol or protocol v2 is being used.
 * Leaving room for if the V2 is ever impl.
 */
public class VotifierProtocolDifferentiator extends ByteToMessageDecoder
{

	@Override
	protected void decode( ChannelHandlerContext ctx, ByteBuf buf, List<Object> list ) throws Exception
	{

		int readable = buf.readableBytes();
		buf.retain();
		buf.readerIndex( 0 );

		VotifierSession session = ctx.channel().attr( VotifierSession.KEY ).get();

		if ( readable == 256 ) {
			// 256 bytes = Protocol v1 Vote Message
			ctx.pipeline().addAfter( "protocolDifferentiator", "protocol1Handler", new VotifierProtocol1Decoder() );
			session.setVersion( VotifierSession.ProtocolVersion.ONE );
			ctx.pipeline().remove( this );
		}
		else {
			throw new CorruptedFrameException( "Unrecognized protocol (missing 0x733A header or 256-byte v1 block)" );
		}
	}
}
