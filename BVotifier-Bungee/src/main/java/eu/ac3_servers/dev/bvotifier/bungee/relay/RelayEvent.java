package eu.ac3_servers.dev.bvotifier.bungee.relay;

import eu.ac3_servers.dev.bvotifier.bungee.model.Vote;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.plugin.Event;

/**
 * @author Cory Redmond
 *         Created by acech_000 on 16/07/2015.
 */
@RequiredArgsConstructor
public class RelayEvent extends Event
{

	@Getter
	public final Vote vote;

}
